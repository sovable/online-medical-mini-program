// components/socket-top/socket-top.js
let app = getApp();
Component({
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      this.setData({
        btnInfo:app.globalData.Custom,
        systemInfo:{
          windowHeight:app.globalData.windowHeight,
          windowWidth:app.globalData.windowWidth,
          CustomBar:app.globalData.CustomBar,
          StatusBar:app.globalData.StatusBar
        }
      });
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  /**
   * 组件的属性列表
   */
  properties: {
    touserName:{
      type:String,
      value:'医生'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    btnInfo:{},
    systemInfo:{}
  },

  /**
   * 组件的方法列表
   */
  methods: {
    goback(){
      wx. navigateBack({
        delta:1
      })
    }
  }
})
