// components/nav-header/index-nav-header.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    to(e) {
      console.log(e.currentTarget.dataset.name);
      switch (e.currentTarget.dataset.name) {
        case "挂号":
          wx.navigateTo({
            url: '/pages/feature/register/register',
          })
          break;
        case "预约":
          wx.navigateTo({
            url: '/pages/feature/reservation/reservation',
          })
          break;
        case "评分":
          wx.navigateTo({
            url: '/pages/feature/score/score',
          })
          break;
        case "更多":
          wx.showToast({
            title: '敬请期待',
          })
          break;
      }
    }
  }
})