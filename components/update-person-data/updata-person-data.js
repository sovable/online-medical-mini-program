// components/update-person-data/updata-person-data.js
import {
  areaList
} from '@vant/area-data';
import uploadFile from '../../utils/uploadFile';
let app =getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    persondata:{
      type:Object,
      value:{}
    }
  },
  lifetimes: {
    attached() {
      // 进来遍历年龄选择器
      for (let i = 1; i <= 100; i++) {
        this.data.columns.push(i);
        this.setData({
          columns: this.data.columns
        })
      }
    },
    ready	(){
      
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    // persondata: {
    //   userNickname: "",
    //   userGender: '',
    //   userAvatar: "",
    //   userAge: '',
    //   userProvince: '',
    //   userCity: '',
    //   userCountry: '',
    //   userPhone: '',
    //   userAuthentication: '',
    //   userGrade: '',
    //   userGraduate: ''
    // },
    showsex: false,
    showage: false,
    showpho: false,
    showadd: false,
    showUserGrade: false,
    showAuthentication: false,
    showUserGraduate: false,
    oreadysex: '',
    // 年龄
    columns: [],
    phovalue: '',
    Autvalue: '',
    Gravalue: "",
    Graduatevalue: '',
    //城市
    areaList
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 性别弹出层
    onclose() {
      this.setData({
        showsex: !this.data.showsex,
        'persondata.userGender': this.data.oreadysex == '男' ? 1 : 0
      })
    },
    // 年龄电话弹出层
    openAgeShow() {
      this.setData({
        showage: !this.data.showage,
      })
    },
    // 电话弹出层
    openShowPho() {
      this.setData({
        showpho: !this.data.showpho,
      })
    },
    // 身份弹出层
    openShowAuthentication() {
      this.setData({
        showAuthentication: !this.data.showAuthentication,
      })
    },
    // 评分弹出
    openShowUserGrade() {
      this.setData({
        showUserGrade: !this.data.showUserGrade,
      })
    },
    // 毕业学校弹出
    openShowUserGraduate() {
      this.setData({
        showUserGraduate: !this.data.showUserGraduate,
      })
    },
    // 地址选择层弹出
    openAddress() {
      this.setData({
        showadd: !this.data.showadd,
      })
    },
    // 获取头像
    bindchooseavatar(e) {
      let that = this
      uploadFile({
        filePath: e.detail.avatarUrl,
        dir: "images/",
        success: function (res) {
          console.log(res);
          that.setData({
          
            'persondata.userAvatar': "https://img.elysia.fit/" + res,
          })

          // console.log("lijing:",this.persondata.userAvatar);
        },
        fail: function (res) {
          console.log("上传失败")
          console.log(res)
        }
      })


    },
    // 修改昵称时如果使用微信的名字开发者工具检查不到
    onInputChange(e) {
      // 在真机上才能生效。开发者工具有bug
      this.setData({
        'persondata.userNickname': e.detail.value,
      })
    },
    // 性别
    onGenderChange(e) {
      this.setData({
        oreadysex: e.currentTarget.dataset.sex
      });
    },

    // 年龄确认
    onconfirm(e) {
      console.log(e.detail.value);
      this.setData({
        'persondata.userAge': e.detail.value
      });
      this.openAgeShow()
    },
    // 电话号码确认
    confirmPho() {
      this.setData({
        'persondata.userPhone': this.data.phovalue
      });
      this.openShowPho()
    },
    // 身份确认
    confirmAut() {
      this.setData({
        'persondata.userAuthentication': this.data.Autvalue
      });
      this.openShowAuthentication()
    },
    // 评分确认  
    confirmGra() {
      this.setData({
        'persondata.userGrade': this.data.Gravalue
      });
      this.openShowUserGrade()
    },
    // 毕业学校确认
    confirmGraduate(){
      this.setData({
        'persondata.userGraduate': this.data.Graduatevalue
      });
      this.openShowUserGraduate()
    },
    // 地址确认
    confirmAddress(e) {
      this.data.persondata.userProvince = '',
        this.data.persondata.userCity = '',
        this.data.persondata.userCountry = '',
        console.log(e.detail.values);

      this.setData({
        'persondata.userProvince': e.detail.values[0].name,
        'persondata.userCity': e.detail.values[1].name,
        'persondata.userCountry': e.detail.values[2].name
      });
      this.openAddress();
    },
    // 取消地址选择
    cancelAddress() {
      this.openAddress();
    },
    // 像服务器发起请求改个人信息
    savePersonData() {
      let userId=this.properties.persondata.userId;

      wx.showLoading({
        title: '正在修改',
        mask:true
      })
    //更新
    wx.request({
      url: app.globalData.url +"/user/update?id="+userId,
      //更新的表单：
      data: this.properties.persondata,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log(res);
        wx.hideLoading();
        wx.showToast({
          title: '修改成功',
        });
        wx.navigateTo({
          url: '/pages/my/user/user',
        })
      },
    })
    },
  }
})