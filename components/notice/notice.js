// colorui/components/notice/notice.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 两文本的内容间隔
    interval_widgth: {
      type: Number,
      value: 10
    },
    scrollText:{
      type:String,
      value:'这里是需要无缝滚动的文本，用于检测滚动效果是否正常！'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

    moveleft:0,
    moveleft2:0,
    text_width:0,
    timer:null
  },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  lifetimes: {
    attached() {
    },
    ready() {
      // 获取内容的宽度
      const query = wx.createSelectorQuery().in(this);
      query.select('.scroll-text').boundingClientRect().exec((res) => {
        this.setData({
          'text_width':res[0].width,
          moveleft2: res[0].width+this.data['interval_widgth']
        })
      });
    },
    detached(){
      clearInterval(this.data.timer);
    }
  },
  pageLifetimes: {
    show() {
      this.data.timer=setInterval(()=>{
        if(this.data.moveleft>=this.data.text_width+this.data.interval_widgth){
          this.setData({
            moveleft:-(this.data.interval_widgth+this.data.text_width)
          })
        };
        if(-(this.data.moveleft2)>=this.data.text_width+this.data.interval_widgth){
          this.setData({
            moveleft2:(this.data.interval_widgth+this.data.text_width)
          })
        };
        this.setData({
          moveleft:this.data.moveleft+2,
          moveleft2:this.data.moveleft2-2
        })
      },20)
     
    },
    hide(){
      clearInterval(this.data.timer);
    }
  }
})