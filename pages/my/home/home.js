const app = getApp();
Page({
  data: {
    userinfo: wx.getStorageSync("userInfo") || {
      userId: '',
      userNickname: "微信用户",
      userGender: '',
      userAvatar: "/image/user.png",
      userAge: '',
      userProvince: '',
      userCity: '',
      userCountry: '',
      userPhone: '',
      createTime: '',
      role:[]
    },
    cooike: "",
    menulist:[],
    
  },
  // 没登陆点击登录
  gologin() {
    app.login("登录中~",
      // 成功后获取用户信息和cooike到home页面
      () => {
      this.setData({
        userinfo: wx.getStorageSync("userInfo"),
        cooike: wx.getStorageSync("token"),
      });
      app.power();
        if(app.globalData.role=="admin"){
          this.setData({
            isdoc:"超级管理员"
          })
        }else if(app.globalData.role=="doctor"){
          this.setData({
            isdoc:"医生VIP"
          })
        }else if(app.globalData.role=="common"){
          this.setData({
            isdoc:"普通用户"
          })
        };
    },
    // 获取菜单页
    ()=>{
      this.setData({
        menulist: app.globalData.menulist,
      })
    }
    );

  },

  //获取用户身份
  getRole(){
    app.power();
    if(app.globalData.role=="admin"){
      this.setData({
        isdoc:"超级管理员"
      })
    }else if(app.globalData.role=="doctor"){
      this.setData({
        isdoc:"医生VIP"
      })
    }else if(app.globalData.role=="common"){
      this.setData({
        isdoc:"普通用户"
      })
    };
  },

  // 退出登录
  outlogin(){
    wx.clearStorage();
    app.outlogin();
    this.onShow();
  },
  toNav(event){
      //跳转界面
      console.log("event",event.currentTarget.dataset.path);
      wx.navigateTo({
        url: event.currentTarget.dataset.path
      })
  },

  onShow: function (options) {
    this.setData({
      userinfo:app.globalData.userinfo,
      cooike:app.globalData.cooike,
      menulist:app.globalData.menulist
    })
    app.getmenu(()=>{
      this.setData({
        menulist: app.globalData.menulist,
      })
    }
      );

      this.getRole()
      console.log("role:",this.data.userinfo.role);

      // let a=app.checkIsDoctor(this.data.userinfo.userId);
      // if(a!=-1){
      //   this.setData({
      //     isdoc:"医生VIP"
      //   })
      // }else{
      //   this.setData({
      //     isdoc:"普通用户"
      //   })
      // };
  },

  // 跳转资料
  setInformation(){
    wx.navigateTo({
      url: '/pages/my/personal-data/person-data',
    })
  }
})