// pages/my/user/updata-person/updata-person.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let that=this
    //查询该用户信息
    wx.request({
      url: app.globalData.url + "/user/query?id=" + options.userId,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log(res);
        that.setData({
          userinfo:res.data.data.user
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})