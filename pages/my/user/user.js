// pages/my/user/user.js
const app = getApp();
Page({
  data: {
    userList: [{
      id: 1,
      nickName: '张三',
      gender: '男',
      avatarUrl: 'https://xxx.com/avatar1.jpg',
    }, ],
    count: '', //人数
    userNickName: "",
    // 需要修改权限的id
    updateBYid:0,
    show: false,
    columns: ["超级管理员","医生","普通用户"],
  },
  onShow() {
    this.getUserList("");
  },
  // 关闭遮罩层
  onClosep() {
    this.setData({
      show: !this.data.show
    })
  },
  //获取用户列表
  getUserList(e) {
    var that = this
    wx.request({
      url: app.globalData.url + "/user/List/query?userNickname=" + e,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log("search:", res);
        that.setData({
          userList: res.data.data.usersList,
          count: res.data.data.count
        })
      },
      fail() {
        wx.showToast({
          title: '还没有登录~',
          icon: 'error'
        })
      },
    })
  },

  onSearch: function (event) {
    this.getUserList(event.detail.value)
  },

  //更新用户信息
  updateUser(event) {
    var userId = event.currentTarget.dataset.id;
    var that = this
    wx.navigateTo({
      url: '/pages/my/user/updata-person/updata-person' + '?userId=' + userId,
    })
  },

  //更新用户角色
  updatePower(event) {
    //查询权限（角色
    var userId = event.currentTarget.dataset.id;
    this.setData({
      updateBYid:userId
    })
    var that = this
    wx.request({
      url: app.globalData.url + "/user/role/queryById?id=" + userId,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log(res);
        //TODO 
        //弹出角色表单（3个复选框-超级管理员-医生-普通用户）
        //原本有几个渲染几个，查询勾选复选框提交,提交一个res打印的格式，只要id就行
        //  let sysRoles=[
        //  { id: 1},
        //  { id: 2 },
        //  { id: 3}]
        // res.data.data.forEach((element) => {
        //   that.data.columns.push(element.name)
        // });
        // that.setData({
        //   columns: that.data.columns
        // });
        that.setData({
          show: !that.data.show
        })
    
      },
    })
  },


  //删除
  onDelete: function (event) {
    var userId = event.currentTarget.dataset.id;
    var that = this
    wx.request({
      url: app.globalData.url + "/user/delete?id=" + userId,

      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        that.getUserList("")
      },

    })

    var userId = event.currentTarget.dataset.id;
    var userList = this.data.userList.filter(function (item) {
      return item.id != userId;
    });
    this.setData({
      userList: userList,
    });
  },
  // 权限选择
  onConfirm(e) {
    let that=this;
    let role=null;
    // 处理值
    switch (e.detail.value) {
      case "超级管理员":
         role=[{id:1},{id:2},{id:3}]
        break;
      case "普通用户":
         role=[{id:3}]
        break;
      case "医生":
         role=[{id:2},{id:3}]
        break;
    }
    let sysRoles=[
      ...role
      ];
      wx.request({
        url: app.globalData.url + '/user/role/update'+'?id='+this.data.updateBYid,
        data:sysRoles,
        method:"POST",
        header:{
          "authorization": wx.getStorageSync("token")
        },
        success(res){
          console.log(res);
          if(res.data.code==200){
            that.setData({
              show: !that.data.show,
              columns: []
            });
            that.getUserList("");
            wx.showToast({
              title: '权限修改成功',
            })
          }
        }
      })
  },
  // 权限取消  // 关闭遮罩层弹窗
  onCancel() {
    this.setData({
      show: !this.data.show,
     
    })
  },
});