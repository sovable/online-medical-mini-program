// pages/my/personal-data/person-data.js
import {
  areaList
} from '@vant/area-data';

import uploadFile from '../../../utils/uploadFile';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    persondata: {
    
      userNickname: "",
      userGender: '',
      userAvatar: "",
      userAge: '',
      userProvince: '',
      userCity: '',
      userCountry: '',
      userPhone: '',
    
    },
    showsex: false,
    showage: false,
    showpho: false,
    showadd: false,
    oreadysex: '',
    // 年龄
    columns: [],
    value: '',
    //城市
    areaList
  },
 


  // 性别弹出层
  onclose() {
    this.setData({
      showsex: !this.data.showsex,
      'persondata.userGender': this.data.oreadysex=='男'?1:0
    })
  },
  // 年龄电话弹出层
  openAgeShow() {
    this.setData({
      showage: !this.data.showage,
    })
  },
  // 电话弹出层
  openShowPho() {
    this.setData({
      showpho: !this.data.showpho,
    })
  },
  // 地址选择层弹出
  openAddress() {
    this.setData({
      showadd: !this.data.showadd,
    })
  },
  // 获取头像
  bindchooseavatar(e) {
    let that=this
    

    
    uploadFile(
      {
        filePath: e.detail.avatarUrl,
        dir: "images/",
        success: function (res) {
          
          that.setData({
            'persondata.userAvatar': "https://img.elysia.fit/"+res,
          })

         // console.log("lijing:",this.persondata.userAvatar);
        },
        fail: function (res) {
          console.log("上传失败")
          console.log(res)
        }
      })

   
  },
  // 修改昵称时如果使用微信的名字开发者工具检查不到
  onInputChange(e) {
    // 在真机上才能生效。开发者工具有bug
    this.setData({
      'persondata.userNickname': e.detail.value,
    })
  },
  // 性别
  onGenderChange(e) {
    this.setData({
      oreadysex: e.currentTarget.dataset.sex
    });
  },

  // 年龄确认
  onconfirm(e) {
    console.log(e.detail.value);
    this.setData({
      'persondata.userAge': e.detail.value
    });
    this.openAgeShow()
  },
  // 电话号码确认
  confirmPho() {
    this.setData({
      'persondata.userPhone': this.data.value
    });
    this.openShowPho()
  },
  // 地址确认
  confirmAddress(e) {
    this.data.persondata.userProvince='',
    this.data.persondata.userCity='',
    this.data.persondata.userCountry='',
    console.log(e.detail.values);
   
      this.setData({
        'persondata.userProvince':e.detail.values[0].name,
        'persondata.userCity':e.detail.values[1].name,
        'persondata.userCountry':e.detail.values[2].name
      });
    this.openAddress();
  },
  // 取消地址选择
  cancelAddress() {
    this.openAddress();
  },
  // 像服务器发起请求改个人信息
  savePersonData(){
    wx.request({
      url: getApp().globalData.url+'/user/change',
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      data:this.data.persondata,
      success(res){
        getApp().login( "正在修改~", () => {
          wx.showToast({
            title: '修改成功！',
          })
        },
        // 获取菜单页
        ()=>{
        });

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let userinfo = wx.getStorageSync("userInfo");
    console.log(userinfo);
    this.setData({
      'persondata.userAvatar': userinfo.userAvatar,
      'persondata.userNickname': userinfo.userNickname,
      'persondata.userGender': userinfo.userGender ,
      'persondata.userAge': userinfo.userAge,
      'persondata.userPhone': userinfo.userPhone,
      'persondata.userProvince': userinfo.userProvince ,
      'persondata.userCity': userinfo.userCity ,
      'persondata.userCountry': userinfo.userCountry,
      oreadysex: userinfo.userGender == 1 ? '男' : '女',
    })
    // 进来遍历年龄选择器
    for (let i = 1; i <= 100; i++) {
      this.data.columns.push(i);
      this.setData({
        columns: this.data.columns
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})