const app = getApp();

import bData from '../../service/mock.js'
Page({
  data: {
    contentHeight: app.globalData.windowHeight,
    doctors: [],
    gradelist:[],
    toUserId: '',
    toUsername: '',
    SessionId: '',
    touserAvatar: '',
    // 随机数
    num:Math.floor((Math.random()*4)+3)
  },
  // 页面加载
  onLoad: function (options) {
    this.getDoctorList();

  },
  //获取全部医生
  getDoctorList() {
    var that = this
    wx.request({
      url: app.globalData.url + '/user/doctorList',
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log(res.data.data.UsersList);
        that.setData({
          doctors: res.data.data.UsersList
        })
      },
    })
  },
    //获取评分前5医生
  getGradeDoctorList() {
      var that = this
      wx.request({
        url: app.globalData.url + '/user/list/grade',
        method: "POST",
        header: {
          "authorization": wx.getStorageSync("token")
        },
        success(res) {
          console.log("res.data.data.usersList",res.data.data.usersList);
          that.setData({
            gradelist: res.data.data.usersList
          })
        },
      })
  },

  onShow() {
    this.getDoctorList();
    this.getGradeDoctorList()
  },

  // 拉触底加载
  onReachBottom: function () {
    if (this.data.pageNum != this.data.AllPage) {
      console.log('没触底')
      var pageNo = this.data.pageNum + 1;
      this.GetArticles(pageNo);
      this.setData({
        pageNum: pageNo
      })
    } else {
      console.log('触底了')
    }
  },


  //跳转聊天界面
  toChat(event) {
    var that = this
    console.log('event', event);
    that.setData({
      toUserId: event.currentTarget.dataset.id,
      toUsername: event.currentTarget.dataset.name,
      touserAvatar: event.currentTarget.dataset.useravatar
    })

    this.createSession()

  },

  // 创建会话
  createSession() {
    let that = this
    console.log("创建会话")
    console.log(that.data.toUserId);
    wx.request({
      url: app.globalData.url + '/createSession?id=' + app.globalData.userinfo.userId + "&userNickname=" + app.globalData.userinfo.userNickname + "&toUserId=" + that.data.toUserId + "&toUsername=" + that.data.toUsername,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log("创建会话res:", res);
        that.setData({
          SessionId: res.data.data.id
        })
        console.log("创建会话sessionid:", that.data.SessionId);
        //跳转界面
        wx.navigateTo({
          url: '/pages/websocket/socket?toUserId=' + that.data.toUserId + "&toUsername=" + that.data.toUsername + '&SessionId=' + that.data.SessionId + '&toUserAvatar=' + that.data.touserAvatar
        })
      },
      fail() {
        wx.showToast({
          title: '还没有登录~',
          icon: 'error'
        })
      },
    })

  },

})