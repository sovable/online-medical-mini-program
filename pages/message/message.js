// pages/message/message.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    SessionList: [],
    toUsername: '',
    toUserId: '',
    sessionId: '',
    userId: ''
  },
  getSessionList() {
    var that = this
    wx.request({

      url: app.globalData.url + '/sessionList/already?id=' + app.globalData.userinfo.userId,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log("getSessionList:", res);
        that.setData({
          SessionList: res.data.data
        });
        if(res.data.code==403) {
          wx.hideLoading();
          return;
        }
        
        // that.data.SessionList.forEach((item,index)=> {
        //  if(app.checkIsDoctor(item.userId)!=-1){
        //   // 是医生
        //   that.data.SessionList[index].isdoc=true;
        //  }else{
        //   that.data.SessionList[index].isdoc=false;
        //  }
        //  that.setData({
        //    SessionList:that.data.SessionList
        //  })
        // });
          wx.hideLoading();
      },
      fail() {
        wx.showToast({
          title: '还没有登录~',
          icon: 'error'
        })
      },
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.showLoading({
      title: '数据加载中',
      mask:true
    });
    this.getSessionList()
    this.setData({
      hide: app.globalData.cooike == '' ? true : false
    });
    
  },
  //跳转聊天界面
  toChat(event) {
    var that = this
    that.setData({
      toUserId: event.currentTarget.dataset.id,
      toUsername: event.currentTarget.dataset.name,
      touserAvatar: event.currentTarget.dataset.useravatar
    })
    this.createSession()
  },
  // 创建会话
  createSession() {
    let that = this
    console.log("创建会话")

    wx.request({
      url: app.globalData.url + '/createSession?id=' + app.globalData.userinfo.userId + "&userNickname=" + app.globalData.userinfo.userNickname + "&toUserId=" + that.data.toUserId + "&toUsername=" + that.data.toUsername,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log("创建会话res:", res);
        that.setData({
          SessionId: res.data.data.id
        })
        console.log("创建会话sessionid:", that.data.SessionId);
        //跳转界面
        wx.navigateTo({
          url: '/pages/websocket/socket?toUserId=' + that.data.toUserId + "&toUsername=" + that.data.toUsername + '&SessionId=' + that.data.SessionId + '&toUserAvatar=' + that.data.touserAvatar
        })
      },
      fail() {
        wx.showToast({
          title: '还没有登录~',
          icon: 'error'
        })
      },
    })

  },
  //删除会话
  removeMsg(e) {
    let that = this
    console.log(e);
    wx.request({

      url: app.globalData.url + '/delSession?sessionId=' + e.currentTarget.dataset.id + "&userId=" + e.currentTarget.dataset.fromid + "&toUserId=" + e.currentTarget.dataset.touserid,

      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        that.getSessionList()
      },
      fail() {
        wx.showToast({
          title: '还没有登录~',
          icon: 'error'
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

   this.getSessionList();
   this.setData({
    hide: app.globalData.cooike == '' ? true : false
  });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})