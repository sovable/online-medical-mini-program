// pages/feature/register/choosedoc/choosedoc.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    //后端查库看哪个时间段没有人预约
    columns: ["15:00", "16:00", "17:00"],
    user:'',//点击预约的医生信息
    date:''
    
  },
  changetimer() {
    this.setData({
      show: !this.data.show
    })
  },
  cancel() {
    this.changetimer()
  },
  //确定预约
  confirm(e) {
    let that=this
    wx.showLoading({
      title: "正在预约中",
      mask: true
    });

    let form={
      userId:app.globalData.userinfo.userId,
      doctorId:this.data.user.userId,
      appointmentTime:this.data.date+" "+e.detail.value
    }



    //提交后端
    wx.request({
      url: app.globalData.url + "/appointment/add",
      method: "POST",
      data:form,
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {

        setTimeout(() => {
                // 关闭提示框
        //  wx.hideLoading();

         wx.showToast({
          title: '预约成功',
          mask: true
        });

         that.changetimer();
         wx.navigateTo({
           url: '/pages/feature/reservation/reservation',
         })
         
        }, 3000);
   
      },
   
    })

 
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log("options",options);

    this.setData({
      columns:JSON.parse(options.time),
      user:JSON.parse(options.user),
      date:options.date

    })

   // console.log("options",this.data.columns);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})