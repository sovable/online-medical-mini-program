// pages/feature/register/register.js
let timer= require("../../../utils/timer");
let app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeindex:0,
    dayarr:[],
    weekarr:[],
    screenHeight: 0,
    CustomBar:app.globalData.CustomBar,
    doctors:[],
    date:''
  },

  /**
   * 生命周期函数--监听页面加载
   */



  onLoad(options) {
    

   
    let that=this;
    this.setData({
      dayarr:timer.timerarr().arr,
      weekarr:timer.timerarr().weekarr
    });

    let now=new Date()
    let month=now.getMonth()+1>9?now.getMonth()+1:"0"+(now.getMonth()+1)
    let day=this.data.dayarr[0]>9?this.data.dayarr[0]:"0"+this.data.dayarr[0]
    let time=now.getUTCFullYear()+"-"+month +"-"+day

    this.setData({
      date:time
    });
    this.getAvaDoctorList(time)

    wx.getSystemInfo({
      success(e){
        that.setData({
          screenHeight:e.screenHeight
        })
      }
    });
  },

 //获取可预约医生列表
 getAvaDoctorList(time) {


  var that = this
  wx.request({
    url: app.globalData.url + '/appointment/time/'+time,
    method: "GET",
    header: {
      "authorization": wx.getStorageSync("token")
    },
    success(res) {
     console.log(res);
      that.setData({
        doctors: res.data.data
      })
    },
  })
},

  changeActive(e){

    let now=new Date()
    let month=now.getMonth()+1>9?now.getMonth()+1:"0"+(now.getMonth()+1)
    let day=this.data.dayarr[e.currentTarget.dataset.index]>9?this.data.dayarr[e.currentTarget.dataset.index]:"0"+this.data.dayarr[e.currentTarget.dataset.index]

    let time=now.getUTCFullYear()+"-"+month +"-"+day
    this.setData({
      activeindex:e.currentTarget.dataset.index,
      date:time
    });
console.log("可预约:",this.data.date);
this.getAvaDoctorList(time)

    

    
    // 获取医生列表  根据时间查询医生列表



  },
  // 点击医生跳转到预约信息页
  goregister(e){
   
    let time= JSON.stringify(e.currentTarget.dataset.time)
    let user= JSON.stringify(e.currentTarget.dataset.user)

   
    wx.navigateTo({
      url: '/pages/feature/register/choosedoc/choosedoc?time='+time+"&user="+user+"&date="+this.data.date,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})