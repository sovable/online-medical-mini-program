// pages/feature/reservation/reservation.js
let app= getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    screenHeight: 0,
    CustomBar:app.globalData.CustomBar,

   appointment:[],//预约数据
   userinfo:app.globalData.userinfo

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getAppointment()
    let that=this;
    wx.getSystemInfo({
      success(e){
        that.setData({
          screenHeight:e.screenHeight,
          screenWidth:e.screenWidth
        })
      }
    });

    console.log("len:",this.data.appointment);





  },

getAppointment(){
  let that=this
  //获取预约信息
wx.request({
  url: app.globalData.url + "/appointment/"+app.globalData.userinfo.userId,
  method: "GET",
  header: {
    "authorization": wx.getStorageSync("token")
  },
  success(res) {
    console.log("res:",res.data.data);
   
    that.setData({
     appointment:res.data.data


    })

  
  },

})
},
//取消预约
closeDo(e){
  let that=this
//获取预约信息
wx.request({
  url: app.globalData.url + "/appointment/del/"+e.currentTarget.dataset.id,
  method: "POST",
  header: {
    "authorization": wx.getStorageSync("token")
  },
  success(res) {
    console.log("res:",res);
    //跳转界面
    that.getAppointment()
  
  },

})

},
//结束预约
endDo(e){
  let that=this

wx.request({
  url: app.globalData.url + "/appointment/update/"+e.currentTarget.dataset.id,
  method: "POST",
  header: {
    "authorization": wx.getStorageSync("token")
  },
  success(res) {
    console.log("res:",res);
    //跳转界面
    that.getAppointment()
  
  },

})
},
//评分
goScore(e){
  console.log(e);
//打开评分界面
wx.navigateTo({
  url: '/pages/feature/score/endreservation/endres?id='+e.currentTarget.dataset.id+'&name='+e.currentTarget.dataset.name+'&timer='+e.currentTarget.dataset.timer,
})
console.log(e.currentTarget.dataset.id);
let that=this;

},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getAppointment()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})