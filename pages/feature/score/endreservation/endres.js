// pages/feature/score/endreservation/endres.js
let app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value:0,
    id:0,
    name:'',
    timer:''
  },
  onChange(event) {
    this.setData({
      value: event.detail,
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options.id);
    this.setData({
      id:options.id,
      name:options.name,
      timer:options.timer
    })
  },
  confirm(){
    if (this.data.value==0) {
      wx.showToast({
        title: '请选择分数',
        icon:'error'
      })
    }else{
    //提交评分
  wx.request({
    //app.globalData.url + "/appointment/update/"+e.currentTarget.dataset.id+"/"+score
    url: app.globalData.url + "/appointment/update/"+this.data.id+"/"+this.data.value*2,
    method: "POST",
    header: {
      "authorization": wx.getStorageSync("token")
    },
    success(res) {
      console.log("res:",res);
      //跳转界面
      wx.redirectTo({
        url: '/pages/feature/reservation/reservation',
      })
    
    },
  })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})