// pages/feature/score/score.js
let app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role:'',
    screenHeight: 0,
    CustomBar:app.globalData.CustomBar,
    // 医生版的评分
    scoreList: [],
    username:[],
    // 用户版的评分
    userscoreList:[],
    userinfo:app.globalData.userinfo
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let that=this;
app.power();
this.setData({
  role:app.globalData.role
})
    wx.getSystemInfo({
      success(e){
        that.setData({
          screenHeight:e.screenHeight,
          screenWidth:e.screenWidth
        })
      }
    });
    //获取医生评分信息
  wx.request({
    url: app.globalData.url + "/appointment/doctor/score/"+app.globalData.userinfo.userId,
    method: "GET",
    header: {
      "authorization": wx.getStorageSync("token")
    },
    success(res) {
      console.log("res:",res.data.data);
     
      that.setData({
        scoreList:res.data.data
      })
  // 获取评分人的姓名
  res.data.data.forEach(element => {
    wx.request({
      url: app.globalData.url + "/user/query?id="+res.data.data[0].userId,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        console.log("res:",res);
       that.data.username.push(res.data.data.user.userNickname);
        that.setData({
          username:that.data.username
        })
      
      },
    
    });
  });
  
  // 获取用户给谁评分
  wx.request({
    url: app.globalData.url + "/appointment/score/"+app.globalData.userinfo.userId,
    method: "GET",
    header: {
      "authorization": wx.getStorageSync("token")
    },
    success(res) {
      console.log("res:",res.data.data);
      that.setData({
        userscoreList:res.data.data
      })
    },
  });
  },
});
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})