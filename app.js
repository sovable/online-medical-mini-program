
App({
  globalData: {
    userinfo: wx.getStorageSync("userInfo") || {
      userId: '',
      userNickname: "微信用户",
      userGender: '',
      userAvatar: "/image/user.png",
      userAge: '',
      userProvince: '',
      userCity: '',
      userCountry: '',
      userPhone: '',
      createTime: '',
      msg: '',
      role:[]
    },
    // 菜单项
    menulist: [],
    role:'',
    cooike: wx.getStorageSync("token"),

    // 本地
  //  url: "http://127.0.0.1:8081/wx",
  // socketUrl:'ws://127.0.0.1:8081/wx/websocket/'


    // 远程服务器
    url: "https://elysia.fit/wx",
    socketUrl: "wss://elysia.fit/wx/websocket/"


  },

  onLaunch: function () {
   
    let that = this
    wx.getSystemInfo({
      success: e => {
        // 顶部状态
        this.globalData.StatusBar = e.statusBarHeight;
        // 胶囊按钮信息
        let custom = wx.getMenuButtonBoundingClientRect();
        // 自定义顶部
        this.globalData.Custom = custom;
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;
        // 设备可视高宽
        this.globalData.windowHeight = e.windowHeight;
        this.globalData.windowWidth = e.windowWidth;
        console.log(custom);
      }
    });

    //定时器，每3秒刷新一次消息数
    setInterval(function () {
      that.getSessionList()
    }, 3000)
  },
  // 退出登录
  outlogin() {
    this.globalData.userinfo = {},
      this.globalData.cooike = "",
      this.globalData.menulist = []
  },
  // 获取个人中心菜单
  getmenu(cb) {
    if (wx.getStorageSync("token")) {
      let that = this;
      wx.request({
        url: this.globalData.url + '/user/menu',
        method: "POST",
        header: {
          "authorization": wx.getStorageSync("token")
        },
        success(res) {

          if (res.data.code == 200) {
            that.globalData.menulist = res.data.data;
            cb();
          }
        },
        fail() {
          wx.showToast({
            title: '还没有登录~',
            icon: 'error'
          })
        },
      })
    }

  },
  login(title, cb, cbt) {
    let that = this;
    // 进来直接登录
    wx.showLoading({
      title: title,
      mask: true
    });
    wx.login({
      success: (res) => {
        console.log(res);
        if (res.errMsg == "login:ok") {
          wx.request({
            url: this.globalData.url + '/login?code=' + res.code,
            method: "POST",
            data: this.globalData.userinfo,
            success(res) {
              console.log(res);
              // 关闭提示框
              wx.hideLoading();
              // 存信息
              wx.setStorageSync("token", res.data.data.token);
              that.globalData.cooike = res.data.data.token;

              wx.setStorageSync("userInfo", res.data.data.user);
              that.globalData.userinfo = res.data.data.user;
              cb();
              that.getmenu(cbt);
            },
            fail(res) {
              wx.hideLoading();
              wx.showToast({
                title: '登录失败~',
                icon: 'error'
              })
            }

          })
        }
      },
    })
  },



  //全局监听获取消息
  getSessionList() {
    var that = this
    wx.request({
      url: that.globalData.url + '/sessionList/already?id=' + that.globalData.userinfo.userId,
      method: "POST",
      header: {
        "authorization": wx.getStorageSync("token")
      },
      success(res) {
        let array = res.data.data
        let unReadCount = 0
        try {
          array.forEach(e => {
            unReadCount += e.unReadCount
          });
        } catch (error) {
          console.log("这里有小问题但是不影响");
        }
    
        if (unReadCount > 0) {
          that.changeTabBar(unReadCount)
        } else {
          that.defaultTabBar()
        }
      },
    })
  },

  //改变底部未读消息
  changeTabBar: function (e) {
    wx.setTabBarItem({
      index: 1, // 需要修改的tab的下标
      text: "新未读(" + e + ")", // 修改后的文字内容，包含未读消息数量
      iconPath: "images/tabbar/have_tip.png", // 修改后的图标
      selectedIconPath: "images/tabbar/have_tip.png" // 修改后的选中图标
    })
  },
  //恢复默认样式
  defaultTabBar: function () {
    wx.setTabBarItem({
      index: 1, // 需要修改的tab的下标
      text: "消息", // 修改后的文字内容，包含未读消息数量
      iconPath: "images/tabbar/tip.png", // 修改后的图标
      selectedIconPath: "images/tabbar/tip_cur.png" // 修改后的选中图标
    })
  },

//判断用户身份
power(){
  if( this.globalData.userinfo.role.some(item => item.code.includes("common"))){
    this.globalData.role="common"
  }
  if( this.globalData.userinfo.role.some(item => item.code.includes("doctor"))){
    this.globalData.role="doctor"
  }

  if( this.globalData.userinfo.role.some(item => item.code.includes("admin"))){
    this.globalData.role="admin"
  }
 
console.log("权限：",this.globalData.role);
  

}







  // 判断是否是医生
//   getDoctor() {
//     let that = this;
//     wx.request({
//       url: this.globalData.url + '/user/doctorList',
//       method: "POST",
//       header: {
//         "authorization": wx.getStorageSync("token")
//       },
//       success(res) {
      
//         that.globalData.doctors = res.data.data.UsersList;
//       },
//       fail() {
//         wx.showToast({
//           title: '还没有登录~',
//           icon: 'error'
//         })
//       },
//     });
//   },
// checkIsDoctor(id){
//   return this.globalData.doctors.findIndex((item) => {
//     return item.userId == id;
//   });
// }
})